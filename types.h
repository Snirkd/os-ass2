typedef unsigned int   uint;
typedef unsigned short ushort;
typedef unsigned char  uchar;
typedef uint pde_t;

#define SIG_DFL -1 /* default signal handling */ // AVI 2.1.1
#define SIG_IGN 1 /* ignore signal */ // AVI 2.1.1

#define SIGKILL 9 // AVI 2.1.1
#define SIGSTOP 17  // AVI 2.1.1
#define SIGCONT 19  // AVI 2.1.1

typedef void (*sighandler_t)(int); // AVI 2.1.4
