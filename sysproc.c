#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;
  int signum; // AVI 2.2.1

  if(argint(0, &pid) < 0)
    return -1;
  if(argint(1, &signum) < 0) // AVI 2.2.1
    return -1; // AVI 2.2.1
  return kill(pid, signum); // AVI 2.2.1
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

// update the process signal mask, and return the old mask. // AVI 2.1.3
int
sys_sigprocmask(void)
{
  int sigmask = 0;

  if(argint(0, &sigmask) < 0) return -1; // if arg retrieval fails
  return (int)sigprocmask((uint) sigmask);
}

// register a new handler for a given signal number(signal) // AVI 2.1.4
// if failed, -1 is returned. // AVI 2.1.4
// otherwise, a pointer to the previous signal handler is returned. // AVI 2.1.4
int
sys_signal(void)
{
  int signum = 0;
  int handler = 0;

  if(argint(0, &signum) < 0) return -1; // if arg retrieval fails
  if(argint(1, &handler) < 0) return -1; // if arg retrieval fails
  return (int)signal(signum, (sighandler_t)handler);
}

// responsible for restoring the process to its original workflow // AVI 2.1.5
int sys_sigret(void) {
  sigret();
  return 0;
}
