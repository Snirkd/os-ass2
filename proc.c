#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "x86.h"
#include "proc.h"
#include "spinlock.h"

struct {
  struct spinlock lock;
  struct proc proc[NPROC];
} ptable;

static struct proc *initproc;

int nextpid = 1;
extern void forkret(void);
extern void trapret(void);

static void wakeup1(void *chan);

void
pinit(void)
{
  initlock(&ptable.lock, "ptable");
}

// Must be called with interrupts disabled
int
cpuid() {
  return mycpu()-cpus;
}

// Must be called with interrupts disabled to avoid the caller being
// rescheduled between reading lapicid and running through the loop.
struct cpu*
mycpu(void)
{
  int apicid, i;
  
  if(readeflags()&FL_IF)
    panic("mycpu called with interrupts enabled\n");
  
  apicid = lapicid();
  // APIC IDs are not guaranteed to be contiguous. Maybe we should have
  // a reverse map, or reserve a register to store &cpus[i].
  for (i = 0; i < ncpu; ++i) {
    if (cpus[i].apicid == apicid)
      return &cpus[i];
  }
  panic("unknown apicid\n");
}

// Disable interrupts so that we are not rescheduled
// while reading proc from the cpu structure
struct proc*
myproc(void) {
  struct cpu *c;
  struct proc *p;
  pushcli();
  c = mycpu();
  p = c->proc;
  popcli();
  return p;
}


int 
allocpid(void) 
{
  int pid;
  do { // AVI 3.1.1
    pid = nextpid; // AVI 3.1.1
  } while (!cas(&nextpid, pid, pid+1)); // AVI 3.1.1
  return pid;
}


//PAGEBREAK: 32
// Look in the process table for an UNUSED proc.
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
  struct proc *p;
  char *sp;

  // acquire(&ptable.lock); // AVI 3.2

  // for(p = ptable.proc; p < &ptable.proc[NPROC]; p++) // AVI 3.2
  //   if(p->state == UNUSED) // AVI 3.2
  //     goto found; // AVI 3.2

  // release(&ptable.lock); // AVI 3.2
  // return 0; // AVI 3.2

// found: // AVI 3.2
  // p->state = EMBRYO; // AVI 3.2
  // release(&ptable.lock); // AVI 3.2

  pushcli(); // AVI 3.2
  do { // AVI 3.2
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++) // AVI 3.2
      if(p->state == UNUSED) // AVI 3.2
        break; // AVI 3.2
    if (p == &ptable.proc[NPROC]) { // AVI 3.2
      popcli(); // AVI 3.2
      return 0; // ptable is full // AVI 3.2
    } // AVI 3.2
  } while (!cas(&p->state, UNUSED, EMBRYO)); // AVI 3.2
  popcli(); // AVI 3.2

  p->pid = allocpid();


  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
    p->state = UNUSED;
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;

  // Leave room for trap frame.
  sp -= sizeof *p->tf;
  p->tf = (struct trapframe*)sp;

  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
  p->context->eip = (uint)forkret;

  // AVI 2.1.2
  int sh = 0; // AVI 2.1.2
  for(sh = 0; sh < 32; sh++){ // AVI 2.1.2
    p->signalhandlers[sh] = (void*)SIG_DFL; // AVI 2.1.2
  } // AVI 2.1.2

  return p;
}

//PAGEBREAK: 32
// Set up first user process.
void
userinit(void)
{
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];

  p = allocproc();
  
  initproc = p;
  if((p->pgdir = setupkvm()) == 0)
    panic("userinit: out of memory?");
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
  p->sz = PGSIZE;
  memset(p->tf, 0, sizeof(*p->tf));
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
  p->tf->es = p->tf->ds;
  p->tf->ss = p->tf->ds;
  p->tf->eflags = FL_IF;
  p->tf->esp = PGSIZE;
  p->tf->eip = 0;  // beginning of initcode.S

  safestrcpy(p->name, "initcode", sizeof(p->name));
  p->cwd = namei("/");

  // this assignment to p->state lets other cores
  // run this process. the acquire forces the above
  // writes to be visible, and the lock is also needed
  // because the assignment might not be atomic.
  // acquire(&ptable.lock); // AVI 4 // DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD //GGGGGGGGGGGGGGGGGGGGGGGG

  p->state = RUNNABLE;

  // release(&ptable.lock); // AVI 4 // DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD //GGGGGGGGGGGGGGGGGGGGGGGG
}

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
  uint sz;
  struct proc *curproc = myproc();

  sz = curproc->sz;
  if(n > 0){
    if((sz = allocuvm(curproc->pgdir, sz, sz + n)) == 0)
      return -1;
  } else if(n < 0){
    if((sz = deallocuvm(curproc->pgdir, sz, sz + n)) == 0)
      return -1;
  }
  curproc->sz = sz;
  switchuvm(curproc);
  return 0;
}

// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
fork(void)
{
  // cprintf("A FORK TOOK PLACE\n"); // DEBUG // AVI 2.3
  int i, pid;
  struct proc *np;
  struct proc *curproc = myproc();

  // Allocate process.
  if((np = allocproc()) == 0){
    return -1;
  }

  // Copy process state from proc.
  if((np->pgdir = copyuvm(curproc->pgdir, curproc->sz)) == 0){
    kfree(np->kstack);
    np->kstack = 0;
    np->state = UNUSED;
    return -1;
  }
  np->sz = curproc->sz;
  np->parent = curproc;
  *np->tf = *curproc->tf;
  np->signalmask = curproc->signalmask; // AVI 2.1.2
  np->signalmaskbckup = curproc->signalmaskbckup; // AVI 2.1.2
  // *np->signalhandlers = *curproc->signalhandlers; // AVI 2.1.2 // OPTION 1
  int sh = 0; // AVI 2.1.2 // OPTION 2
  for(sh = 0; sh < 32; sh++){ // AVI 2.1.2 // OPTION 2
    np->signalhandlers[sh] = curproc->signalhandlers[sh]; // AVI 2.1.2 // OPTION 2
  } // AVI 2.1.2 // OPTION 2
  np->pendingsignals = 0; // AVI 2.1.2
  np->frozen = 0; // AVI 2.3

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
    if(curproc->ofile[i])
      np->ofile[i] = filedup(curproc->ofile[i]);
  np->cwd = idup(curproc->cwd);

  safestrcpy(np->name, curproc->name, sizeof(curproc->name));

  pid = np->pid;

  // acquire(&ptable.lock); // AVI 4

  // np->state = RUNNABLE; // AVI 4

  // release(&ptable.lock); // AVI 4

  if(!cas(&np->state, EMBRYO, RUNNABLE))
    panic("[in fork] unable to cas...");

  return pid;
}

// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
void
exit(void)
{
  struct proc *curproc = myproc();
  struct proc *p;
  int fd;

  if(curproc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
    if(curproc->ofile[fd]){
      fileclose(curproc->ofile[fd]);
      curproc->ofile[fd] = 0;
    }
  }

  begin_op();
  iput(curproc->cwd);
  end_op();
  curproc->cwd = 0;

  // acquire(&ptable.lock); // AVI 4
  pushcli(); // AVI 4

  // Parent might be sleeping in wait().
  // wakeup1(curproc->parent); // AVI 4

  if(!cas(&curproc->state, RUNNING, NEG_ZOMBIE)) // AVI 4
  	panic("[in exit] unable to cas..."); // AVI 4

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->parent == curproc){
      p->parent = initproc;
      if(p->state == ZOMBIE)
        wakeup1(initproc);
    }
  }

  // Jump into the scheduler, never to return.
  // curproc->state = ZOMBIE; // AVI 4
  sched();
  panic("zombie exit");
}

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int
wait(void)
{
  struct proc *p;
  int havekids, pid;
  struct proc *curproc = myproc();
  
  // acquire(&ptable.lock); // AVI 4
  pushcli(); // AVI 4
  for(;;){
    if (!cas(&curproc->state, RUNNING, NEG_SLEEPING)) { // AVI 4
      panic("[in wait] unable to cas..."); // AVI 4
    } // AVI 4
    curproc->chan = curproc; // AVI 4
    // Scan through table looking for exited children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != curproc)
        continue;
      havekids = 1;
      if(cas(&p->state, ZOMBIE, NEG_UNUSED)){ // AVI 4
        // Found one.
        pid = p->pid;
        // kfree(p->kstack); // AVI 4
        // p->kstack = 0; // AVI 4
        // freevm(p->pgdir); // AVI 4
        p->pid = 0;
        p->parent = 0;
        p->name[0] = 0;
        curproc->chan = 0; // AVI 4
        cas(&curproc->state, NEG_SLEEPING, RUNNING); // AVI 4
        // p->killed = 0; // AVI 4
        // p->state = UNUSED; // AVI 4
        cas(&p->state, NEG_UNUSED, UNUSED); // AVI 4
        // release(&ptable.lock); // AVI 4
        popcli(); // AVI 4
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || curproc->killed){
      curproc->chan = 0; // AVI 4
      if (!cas(&curproc->state, NEG_SLEEPING, RUNNING)) // AVI 4
        panic("[in wait] unable to cas..."); // AVI 4
      // release(&ptable.lock); // AVI 4
      popcli(); // AVI 4
      return -1;
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    // sleep(curproc, &ptable.lock);  //DOC: wait-sleep // AVI 4
    sched(); // AVI 4
  }
}

//PAGEBREAK: 42
// Per-CPU process scheduler.
// Each CPU calls scheduler() after setting itself up.
// Scheduler never returns.  It loops, doing:
//  - choose a process to run
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch back to the scheduler.
void
scheduler(void)
{
  struct proc *p;
  struct cpu *c = mycpu();
  c->proc = 0;
  
  for(;;){
    // Enable interrupts on this processor.
    sti();

    // Loop over process table looking for process to run.
    // acquire(&ptable.lock); // AVI 4
    pushcli(); // AVI 4
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      // if(p->state != RUNNABLE) // AVI 4
      if(!cas(&p->state, RUNNABLE, RUNNING))
        continue;

      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      c->proc = p;
      switchuvm(p);
      // p->state = RUNNING; // AVI 4

      swtch(&(c->scheduler), p->context);
      switchkvm();

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      c->proc = 0;

      if (cas(&p->state, NEG_SLEEPING, SLEEPING)) { // AVI 4
        if (cas(&p->killed, 1, 0)) // AVI 4
          p->state = RUNNABLE; // AVI 4
      } // AVI 4
      if (cas(&p->state, NEG_RUNNABLE, RUNNABLE)) { // AVI 4
      } // AVI 4
      if (p->state == NEG_ZOMBIE) { // AVI 4
        if (!p || p->state != NEG_ZOMBIE)
          panic("freeproc not zombie");
        // if (!p)
        //   panic("freeproc not zombie");
        kfree(p->kstack);
        p->kstack = 0;
        freevm(p->pgdir);
        p->killed = 0;
        p->chan = 0;
        if (cas(&p->state, NEG_ZOMBIE, ZOMBIE)) // AVI 4
          wakeup1(p->parent); // AVI 4
      } // AVI 4
    }
    popcli(); // AVI 4
    // release(&ptable.lock); // AVI 4
  }
}

// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state. Saves and restores
// intena because intena is a property of this
// kernel thread, not this CPU. It should
// be proc->intena and proc->ncli, but that would
// break in the few places where a lock is held but
// there's no process.
void
sched(void)
{
  int intena;
  struct proc *p = myproc();

  // if(!holding(&ptable.lock)) // AVI 4
    // panic("sched ptable.lock"); // AVI 4
  if(mycpu()->ncli != 1)
    panic("sched locks");
  if(p->state == RUNNING)
    panic("sched running");
  if(readeflags()&FL_IF)
    panic("sched interruptible");
  intena = mycpu()->intena;
  swtch(&p->context, mycpu()->scheduler);
  mycpu()->intena = intena;
}

// Give up the CPU for one scheduling round.
void
yield(void)
{
  // acquire(&ptable.lock);  //DOC: yieldlock // AVI 4
  pushcli(); // AVI 4
  // myproc()->state = RUNNABLE; // AVI 4
  if (!cas(&myproc()->state, RUNNING, NEG_RUNNABLE)) // AVI 4
    panic("[in yield] unable to cas..."); // AVI 4
  sched();
  popcli(); // AVI 4
  // release(&ptable.lock); // AVI 4
}

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  popcli(); // AVI 4
  // release(&ptable.lock); // AVI 4

  if (first) {
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot
    // be run from main().
    first = 0;
    iinit(ROOTDEV);
    initlog(ROOTDEV);
  }

  // Return to "caller", actually trapret (see allocproc).
}

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
void
sleep(void *chan, struct spinlock *lk)
{
  struct proc *p = myproc();
  
  if(p == 0)
    panic("sleep");

  if(lk == 0)
    panic("sleep without lk");

  pushcli(); // AVI 4
  p->chan = chan; // AVI 4

  // Go to sleep. // AVI 4
  if (!cas(&p->state, RUNNING, NEG_SLEEPING)) // AVI 4
    panic("[in sleep] unable to cas..."); // AVI 4

  // Must acquire ptable.lock in order to
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  // if(lk != &ptable.lock){  //DOC: sleeplock0
    // acquire(&ptable.lock);  //DOC: sleeplock1 // AVI 4
    release(lk);
  // }
  // // Go to sleep. // AVI 4
  // p->chan = chan; // AVI 4
  // p->state = SLEEPING; // AVI 4

  sched();

  // // Tidy up. // AVI 4
  // p->chan = 0; // AVI 4

  // Reacquire original lock.
  // if(lk != &ptable.lock){  //DOC: sleeplock2
    // release(&ptable.lock); // AVI 4
    acquire(lk);
  // }
  popcli(); // AVI
}

//PAGEBREAK!
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void
wakeup1(void *chan)
{
  struct proc *p;

  // for(p = ptable.proc; p < &ptable.proc[NPROC]; p++) // AVI 4
  //   if(p->state == SLEEPING && p->chan == chan) // AVI 4
  //     p->state = RUNNABLE; // AVI 4

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){ // AVI 4
    if (p->chan == chan && (p->state == SLEEPING || p->state == NEG_SLEEPING)) { // AVI 4
      while (p->state == NEG_SLEEPING) { // AVI 4
        // busy-wait // AVI 4
      } // AVI 4
      if (cas(&p->state, SLEEPING, NEG_RUNNABLE)) { // AVI 4
        p->chan = 0; // AVI 4
        if (!cas(&p->state, NEG_RUNNABLE, RUNNABLE)) // AVI 4
          panic("[in wakeup1] unable to cas..."); // AVI 4
      } // AVI 4
    } // AVI 4
  } // AVI 4
}

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
  // acquire(&ptable.lock); // AVI 4
  pushcli(); // AVI 4
  wakeup1(chan);
  popcli(); // AVI 4
  // release(&ptable.lock); // AVI 4
}

// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid, int signum) // AVI 2.2.1
{
  // cprintf("[proc.c/kill] pid: %d\n", pid); // DEBUG // AVI 2.5
  // cprintf("[proc.c/kill] signum: %d\n", signum); // DEBUG // AVI 2.5
  struct proc *p;

  // // acquire(&ptable.lock); // AVI 4 // DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD //GGGGGGGGGGGGGGGGGGGGGGGG
  // for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
  //   // if(p->pid == pid && p->state != ZOMBIE){
  //   if(p->pid == pid){
  //     // p->killed = 1; // AVI 2.2.1
  //     p->pendingsignals |= 1UL << signum; // AVI 2.2.1
  //     // cprintf("[proc.c/kill] p->pendingsignals: %d\n", (int)p->pendingsignals); // DEBUG // AVI 2.5
  //     // Wake process from sleep if necessary.
  //     // if(p->state == SLEEPING) // AVI 2.2.1 // AVI 4
  //     //   p->state = RUNNABLE; // AVI 2.2.1 // AVI 4
  //     cas(&p->state, SLEEPING, RUNNABLE);
  //     // release(&ptable.lock); // AVI 4
  //     return 0;
  //   }
  // } // DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD //GGGGGGGGGGGGGGGGGGGGGGGG

  do { // AVI 3.2
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++) {// AVI 3.2
      if(p->pid == pid) {// AVI 3.2
       	p->pendingsignals |= 1UL << signum; // AVI 2.2.1 // 4.1
      	return 0; // ptable is full // AVI 3.2
      }
    } // AVI 3.2
  } while (!cas(&p->state, SLEEPING, RUNNABLE)); // AVI 3.2

  // release(&ptable.lock); // AVI 4
  return -1;
}

//PAGEBREAK: 36
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
  static char *states[] = {
  [UNUSED]    "unused",
  [NEG_UNUSED] "neg_unused", // AVI 4
  [EMBRYO]    "embryo",
  [SLEEPING]  "sleep ",
  [NEG_SLEEPING]  "neg_sleep ", // AVI 4
  [RUNNABLE]  "runble",
  [NEG_RUNNABLE]  "neg_runble", // AVI 4
  [RUNNING]   "run   ",
  [ZOMBIE]    "zombie",
  [NEG_ZOMBIE]    "neg_zombie" // AVI 4
  };
  // int i; // AVI 4
  struct proc *p;
  char *state;
  // uint pc[10]; // AVI 4

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
      state = states[p->state];
    else
      state = "???";
    cprintf("%d %s %s", p->pid, state, p->name);
    // if(p->state == SLEEPING){ // AVI 4
    //   getcallerpcs((uint*)p->context->ebp+2, pc); // AVI 4
    //   for(i=0; i<10 && pc[i] != 0; i++) // AVI 4
    //     cprintf(" %p", pc[i]); // AVI 4
    // } // AVI 4
    cprintf("\n");
  }
}

// update the process signal mask, and return the old mask. // AVI 2.1.3
uint
sigprocmask(uint sigmask)
{
  struct proc *curproc = myproc();

  uint oldmask = curproc->signalmask;
  curproc->signalmask = sigmask;
  return oldmask;
}

// register a new handler for a given signal number(signal) // AVI 2.1.4
// if failed, -2 is returned. // AVI 2.1.4
// otherwise, a pointer to the previous signal handler is returned. // AVI 2.1.4
sighandler_t
signal(int signum, sighandler_t handler)
{
  struct proc *curproc = myproc();

  if(signum < 0 || signum > 31) return (sighandler_t)-2;

  sighandler_t oldhandler = (sighandler_t)curproc->signalhandlers[signum];
  curproc->signalhandlers[signum] = handler;
  return oldhandler;
}



// default handler for SIGSTOP // AVI 2.3
void
sigstophandle(void){
  // cprintf("inside sigstophandle function\n"); // DEBUG // AVI 2.3
  struct proc *curproc = myproc();

  curproc->frozen = 1;
  while(((curproc->pendingsignals >> SIGCONT) & 1U) == 0) {
    yield();
  }
  // yield();
}

// default handler for SIGCONT // AVI 2.3
void
sigconthandle(void){
  // cprintf("inside sigconthandle function\n"); // DEBUG // AVI 2.3
  struct proc *curproc = myproc();
  
  curproc->frozen = 0;
}

// default handler for SIGKILL // AVI 2.3
void
sigkillhandle(void){
  // cprintf("inside sigkillhandle function\n"); // DEBUG // AVI 2.3
  struct proc *p = myproc();

  p->killed = 1;
}

// SNIR 2.4
void 
userSpaceHandler(int index)
{
  // Every handler gets the number of the signal
  struct proc *curproc = myproc();
  sighandler_t h = curproc->signalhandlers[index];

  // cprintf("========= userSpaceHandler =========\n");
  // cprintf("Current esp: %p\n", curproc->tf->esp);
  // cprintf("Current eip: %p\n", curproc->tf->eip);

  // Clear space for trapframe backup and then Backup trapframe:
  curproc->tf->esp -= sizeof(struct trapframe);
  curproc->tfbckup = (struct trapframe*)curproc->tf->esp;
  memmove((void*)curproc->tfbckup, (void*)curproc->tf, sizeof(struct trapframe));

  // Inject the call to sigret unto the stack:
  curproc->tf->esp -= (uint)sigret_end -(uint)sigret_begin;
  memmove((void*)curproc->tf->esp, sigret_begin, (uint)sigret_end - (uint)sigret_begin);

  curproc->tf->esp -= sizeof(int); // making space for signum.
  *((int*)curproc->tf->esp) = index; // Push the argument: the signal number.

  curproc->tf->esp -= sizeof(void*); // making space for return address.  
  // fix the stack to point to ret addr;
  *((int*)curproc->tf->esp) = curproc->tf->esp + (sizeof(int) + sizeof(void*)); 
  // setting the call the handler:
  curproc->tf->eip = (uint) h;

  // cprintf("Current esp post mess: %p\n", curproc->tf->esp);

  // cprintf("========= userSpaceHandler =========\n\n");

}

// function for traversing signals and handling them // AVI 2.4
void 
signalshandler(void)
{
  struct proc *curproc = myproc();
  if(curproc == 0){ 
    return;
  }
  // If the context switch wont go to user space, we dont handle the signals now.
  if((curproc->tf->cs & 3) != DPL_USER){
    return;
  }

  // cprintf("In signals hanldler, in proceess name: %s, process number: %d\n", curproc->name, curproc->pid);
  int i = 0;
  for(i = 0; i < 32; i++){
    // cprintf("In signals hanldler loop, in proceess name: %s, process number: %d\n", curproc->name, curproc->pid);
    int blocking = (curproc->signalmask >> i) & 1U;
    // cprintf("In signals hanldler loop 2, in proceess name: %s, process number: %d\n", curproc->name, curproc->pid);
    int pending = (curproc->pendingsignals >> i) & 1U;
    // cprintf("In signals hanldler loop 3, in proceess name: %s, process number: %d\n", curproc->name, curproc->pid);
    sighandler_t h = curproc->signalhandlers[i];
    // cprintf("In signals hanldler loop 4, in proceess name: %s, process number: %d\n", curproc->name, curproc->pid);
    if(blocking == 0){
      if(pending == 1 && (int)h != SIG_IGN){
        curproc->signalmaskbckup = sigprocmask(~0);
        switch(i){
          case SIGSTOP:
            if((int)h == SIG_DFL){
              sigstophandle();
            }else{
              userSpaceHandler(i);
            }
            break;
          case SIGCONT:
            if((int)h == SIG_DFL){
              sigconthandle();
            }else{
              userSpaceHandler(i);
            }
            break;
          case SIGKILL:
            if((int)h == SIG_DFL){
              sigkillhandle();
            }else{
              userSpaceHandler(i);
            }
            break;
          default:
            if((int)h == SIG_DFL){
              sigkillhandle();
            }else{
              userSpaceHandler(i);
            }
            break;
        }
        curproc->signalmaskbckup = sigprocmask(curproc->signalmaskbckup);
      }
      curproc->pendingsignals &= ~(1UL << i);
    } 
  }
}

// responsible for restoring the process to its original workflow // AVI 2.1.5
void
sigret(void)
{
  struct proc *curproc = myproc();

  // cprintf("========= SIGRET =========\n");
  // cprintf("Current Trapframe's esp: %p\n" , curproc->tf->esp);
  // cprintf("Current eip: %p\n", curproc->tf->eip);

  memmove((void*)curproc->tf, (void*)curproc->tfbckup, sizeof(struct trapframe));

  *((uint*)curproc->tf->esp) = curproc->tf->esp + sizeof(int) + ((uint)sigret_end - (uint)sigret_begin);
  curproc->tf->esp += sizeof(struct trapframe);

  curproc->signalmask = curproc->signalmaskbckup;

  // cprintf("Post fixing:\n");
  // cprintf("Current Trapframe's esp: %p\n" , curproc->tf->esp);
  // cprintf("Current eip: %p\n", curproc->tf->eip);

  
  signalshandler();
  // cprintf("post signal handler\n");
  // cprintf("========= SIGRET =========\n\n");

}